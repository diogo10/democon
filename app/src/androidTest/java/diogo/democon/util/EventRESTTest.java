package diogo.democon.util;

import junit.framework.*;
import junit.framework.Assert;

import org.junit.*;
import org.junit.Test;

import java.util.*;

import diogo.democon.model.*;

/**
 *  Test case for EventREST.class
 *
 * Created by diogo on 18/02/16.
 */
public class EventRESTTest extends TestCase {

    EventREST eventREST;

    @Before
    public void setUp() throws Exception {
        eventREST = new EventREST();
    }

    @Test
    public void testGetAllEvents() throws Exception {
        ArrayList<Event> events = eventREST.getAllEvents();
        Assert.assertNotNull(events);
    }

//    @Test
//    public void testAddEvent() throws Exception {
//        Date date = new Date(System.currentTimeMillis());
//        Event event = new Event(122, "Diogo test", date,date);
//        String result = eventREST.addEvent(event);
//        Assert.assertNotNull(result);
//    }

//    @Test
//    public void testDelete() throws Exception {
//        String result = eventREST.delete(151);
//        Assert.assertNotNull(result);
//    }
}