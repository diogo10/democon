package diogo.democon.util;

import android.app.*;
import android.content.*;
import android.view.*;
import android.widget.*;

import java.util.*;

import diogo.democon.*;
import diogo.democon.model.*;

/**
 * Custom event listview item
 *
 * Created by diogohenrique on 18/02/2016.
 */
public class EventListAdapter extends BaseAdapter {

    private final List<Event> list;
    private LayoutInflater inflater;

    //UTILS
    private MyUtils myUtils;

    public EventListAdapter(Activity context, List<Event> list) {
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myUtils = new MyUtils();
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return list != null ? list.size() : 0;
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list != null ? list.get(position) : null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder = null;

        if (view == null) {
            holder = new ViewHolder();
            int layout = R.layout.content_main_item;
            view = inflater.inflate(layout, null);
            view.setTag(holder);
            holder.tv_event = (TextView) view.findViewById(R.id.tv_event);
            holder.tv_when = (TextView) view.findViewById(R.id.tv_when);
            holder.tv_end = (TextView) view.findViewById(R.id.tv_end);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tv_event.setText(list.get(position).getName());
        holder.tv_when.setText("When: " + myUtils.formatDate(list.get(position).getStart(), "EEE, dd MMM yyyy HH:mm"));
        holder.tv_end.setText("End: " + myUtils.formatDate(list.get(position).getEnd(), "EEE, dd MMM yyyy HH:mm"));

        return view;
    }


    static class ViewHolder {
        TextView tv_event, tv_when, tv_end;
    }


}