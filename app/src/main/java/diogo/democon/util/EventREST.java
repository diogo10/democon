package diogo.democon.util;

import android.util.*;

import com.google.gson.*;
import com.google.gson.reflect.*;

import java.lang.reflect.*;
import java.text.*;
import java.util.*;

import diogo.democon.model.*;

/**
 * Class responsible for all methods from the events api
 *
 * Created by diogo on 18/02/16.
 */
public class EventREST {

    private Gson gson;
    private SimpleDateFormat format;

    public EventREST(){
        gson = new Gson();
        format = new SimpleDateFormat("yyy-MM-dd HH:mm:ss",Locale.ENGLISH);// format date
    }

    /**
     * Get All events
     *
     * @return a model Event.class
     * @throws Exception
     */
    public ArrayList<Event> getAllEvents() throws Exception {

        try {
            String result = ServerUtil.get("events");
            //Log.i("app", "getAllEvents: result = " + result);
            Type type = new TypeToken<List<Event>>() {}.getType();
            ArrayList<Event> events = gson.fromJson(result, type);
            return events;

        }catch (Exception e){
            Log.i("app", "getAllEvents: error = " + e.getMessage());
            throw e;
        }
    }

    /**
     * Add event
     *
     * @param - a model Event class
     * @return - a string
     * @throws Exception
     */
    public String add(Event event) throws Exception {

        String result = null;

        try {
            HashMap<String, String> params = new HashMap<>();

            params.put("event[name]",event.getName());
            params.put("event[start]", format.format(event.getStart()));
            params.put("event[end]", format.format(event.getEnd()));

            result = ServerUtil.post("events", params);
            //Log.i("app", "addEvent result = " + result);

            if(result.isEmpty()){
                result = "Error. Please, try again.";
            }else{
                result = "Success!";
            }

            return result;
        }catch (Exception e){
            throw e;
        }
    }

    /**
     * Delete event
     *
     * @param - a model Event class
     * @return - a string
     * @throws Exception
     */
    public String delete(Integer id) throws Exception {
        try {
            String result = ServerUtil.delete("events/" + id);
            //Log.i("app", "delete result = " + result);

            if(result.isEmpty()){
                result = "Error. Please, try again.";
            }else{
                result = "Success!";
            }

            return result;
        }catch (Exception e){
            throw e;
        }
    }

}
