package diogo.democon.util;

import android.app.*;

import com.rollbar.android.*;

/**
 * Created by diogo on 19/02/16.
 */
public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Rollbar.init(this, "a25785a62fbb4945b65d495379fb241f", "production");// Rollcar error tracking
    }
}
