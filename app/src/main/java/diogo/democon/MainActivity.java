package diogo.democon;

import android.content.*;
import android.os.*;
import android.support.annotation.*;
import android.support.design.widget.*;
import android.support.v7.app.*;
import android.support.v7.widget.Toolbar;
import android.text.*;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.*;
import com.codetroopers.betterpickers.datepicker.*;
import com.rollbar.android.*;

import java.text.*;
import java.util.*;

import diogo.democon.fragments.*;
import diogo.democon.model.*;
import diogo.democon.util.*;

/**
 * Main screen
 * <p/>
 * Created by diogohenrique on 18/02/2016.
 */
public class MainActivity extends AppCompatActivity implements DatePickerDialogFragment.DatePickerDialogHandler {

    private EditText et_start, et_start_hour, et_end, et_end_hour, et_name; //dialogs views
    private FloatingActionButton fab;

    //utils
    private MyUtils myUtils = new MyUtils();
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pref = getSharedPreferences("tips", 0);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myUtils.isNetworkConnected(getApplicationContext()))
                    dialogAddEvent();
                else
                    showMessage(getString(R.string.m_no_internet));
            }
        });

        showTip();//show dialog
    }


    /**
     * Show add event dialog
     */
    public void dialogAddEvent() {

        final Date today = new Date(System.currentTimeMillis());
        String hour = myUtils.formatDate(today, "HH:00");

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .autoDismiss(false)
                .title(R.string.add_event)
                .customView(R.layout.dialog_add_event, true)
                .positiveText(android.R.string.yes)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        if (et_name.getText().toString().isEmpty()) {
                            showMessage("Please, add an event name");
                        } else if (et_start.getText().toString().isEmpty()) {
                            showMessage("Please, add an start date");
                        } else if (et_end.getText().toString().isEmpty()) {
                            showMessage("Please, add an end date");
                        } else {
                            //sucess...send the event
                            dialog.dismiss();

                            String start = null, end = null;


                            try {
                                start = et_start.getText().toString() +
                                        " " + et_start_hour.getText().toString();


                                end = et_end.getText().toString() +
                                        " " + et_end_hour.getText().toString();


                                //create a event obj
                                Event event = new Event(new Random().nextInt(20000), et_name.getText().toString(),
                                        myUtils.getSimpleDateFormat()
                                                .parse(start),
                                        myUtils.getSimpleDateFormat()
                                                .parse(end));

                                new AddEventsAsyncTask().execute(event);//send it

                            } catch (ParseException e) {
                                Rollbar.reportException(e, "critical", "ADD EVENT - Error in parse date - start = " + start + " " + end);
                                showMessage(getString(R.string.m_error_in_date));
                            }


                        }
                    }
                })
                .negativeText(android.R.string.no).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();

        View view = dialog.getCustomView();// get the custom dialog view

        //get inputs
        et_name = (EditText) view.findViewById(R.id.et_0);
        et_start = (EditText) view.findViewById(R.id.et_1);
        et_start.setInputType(InputType.TYPE_NULL);
        et_start_hour = (EditText) view.findViewById(R.id.et_1_1);
        et_start_hour.setText(hour);
        et_end = (EditText) view.findViewById(R.id.et_2);
        et_end.setInputType(InputType.TYPE_NULL);
        et_end_hour = (EditText) view.findViewById(R.id.et_2_1);
        et_end_hour.setText(hour);


        //checkbox event
        ((CheckBox) view.findViewById(R.id.checkBox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    et_start_hour.setVisibility(View.GONE);
                    et_end_hour.setVisibility(View.GONE);
                } else {
                    et_start_hour.setVisibility(View.VISIBLE);
                    et_end_hour.setVisibility(View.VISIBLE);
                }
            }
        });

        //show date dialog for start date
        et_start.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    new DatePickerBuilder()
                            .setReference(1)
                            .setFragmentManager(getSupportFragmentManager())
                            .setYear(Integer.valueOf(myUtils.formatDate(today, "yyyy")))
                            .setStyleResId(R.style.BetterPickersDialogFragment).show();
            }
        });

        //show date dialog for end date
        et_end.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    new DatePickerBuilder()
                            .setReference(2)
                            .setFragmentManager(getSupportFragmentManager())
                            .setYear(Integer.valueOf(myUtils.formatDate(today, "yyyy")))
                            .setStyleResId(R.style.BetterPickersDialogFragment).show();
            }
        });


    }

    @Override
    public void onDialogDateSet(int reference, int year, int monthOfYear, int dayOfMonth) {

        DecimalFormat decimalFormat = new DecimalFormat("00");
        if (year < 2000) {
            showMessage("You need to select the long year format");
        } else {

            switch (reference) {

                case 1:
                    et_start.setText(decimalFormat.format(dayOfMonth) + "/" + decimalFormat.format(monthOfYear + 1) + "/" + year);
                    break;

                case 2:
                    et_end.setText(decimalFormat.format(dayOfMonth) + "/" + decimalFormat.format(monthOfYear + 1) + "/" + year);
                    break;

            }

        }


    }


    /**
     * A generic Snackbar message
     *
     * @param message
     */
    private void showMessage(String message) {
        Snackbar.make(fab, message,
                Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    /**
     * Show a info dialog
     */
    private void showTip() {

        if (pref.getBoolean("show_tip_1", true)) {

            new MaterialDialog.Builder(this)
                    .title(R.string.info)
                    .content(R.string.info_content)
                    .positiveText(android.R.string.ok)
                    .negativeText(R.string.fine)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putBoolean("show_tip_1", false);
                            editor.commit();
                        }
                    })
                    .show();

        }

    }

    /**
     * Class responsible to send the event to server
     */
    class AddEventsAsyncTask extends AsyncTask<Event, Integer, String> {

        private EventREST eventREST;
        private MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new MaterialDialog.Builder(MainActivity.this)
                    .title(R.string.sending)
                    .content(R.string.wait)
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(Event... event) {
            eventREST = new EventREST();

            try {
                return eventREST.add(event[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String events) {
            super.onPostExecute(events);
            dialog.dismiss();

            if (events == null)
                Snackbar.make(fab, getResources().getString(R.string.m_server_problem),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            else {
                ((EventFragment) getSupportFragmentManager().getFragments().get(0)).updateList();
            }


        }
    }

}
