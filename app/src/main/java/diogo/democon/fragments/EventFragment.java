package diogo.democon.fragments;

import android.os.*;
import android.support.annotation.*;
import android.support.design.widget.*;
import android.support.v4.app.*;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.*;
import com.daimajia.androidanimations.library.*;
import com.yalantis.phoenix.*;

import java.util.*;

import diogo.democon.R;
import diogo.democon.model.*;
import diogo.democon.util.*;

/**
 * Fragment responsible for list the events
 *
 * Created by diogohenrique on 18/02/2016.
 */
public class EventFragment extends Fragment {

    private ListView listView;
    private EventListAdapter eventListAdapter;
    private PullToRefreshView mPullToRefreshView;

    //utils
    private MyUtils myUtils = new MyUtils();
    private TinyDB tinyDB;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = (ListView) view.findViewById(R.id.listView);
        tinyDB = new TinyDB(getActivity().getApplicationContext());//config my tinyDB

        mPullToRefreshView = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);
        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateList();
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View arg1, int pos, long id) {
                if (myUtils.isNetworkConnected(getActivity())) {
                    Event event = (Event) parent.getAdapter().getItem(pos);
                    new DeleteEventsAsyncTask().execute(event.getId());
                } else {
                    Snackbar.make(getView(), getResources().getString(R.string.m_no_internet),
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                return true;
            }
        });

    }

    /**
     * Aux method that call GetAllEvents.class
     */
    public void updateList() {
        if (myUtils.isNetworkConnected(getActivity())) {
            new GetAllEvents().execute();
        } else {
            Snackbar.make(getView(), getResources().getString(R.string.m_no_internet),
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            mPullToRefreshView.setRefreshing(false);//stop animation

            //show previous data
            eventListAdapter = new EventListAdapter(getActivity(), tinyDB.getListObject("events"));
            listView.setAdapter(eventListAdapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateList();//update list
    }

    /**
     * Class responsible to get events from the server
     */
    class GetAllEvents extends AsyncTask<Void, Integer, List<Event>> {

        private EventREST eventREST;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mPullToRefreshView.setRefreshing(true);
        }

        @Override
        protected List<Event> doInBackground(Void... voids) {
            eventREST = new EventREST();

            try {
                ArrayList<Event> eventList = eventREST.getAllEvents();
                tinyDB.putListObject("events", eventList);//save list
                return eventList;
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(List<Event> events) {
            super.onPostExecute(events);
            mPullToRefreshView.setRefreshing(false);

            if (events == null) {
                Snackbar.make(getView(), getResources().getString(R.string.m_server_problem),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                //show previous data
                eventListAdapter = new EventListAdapter(getActivity(), tinyDB.getListObject("events"));
                listView.setAdapter(eventListAdapter);

            } else {
                eventListAdapter = new EventListAdapter(getActivity(), events);
                listView.setAdapter(eventListAdapter);
                YoYo.with(Techniques.Tada)
                        .duration(700)
                        .playOn(listView);

                Snackbar.make(getView(), "You've " + events.size() + " events.",
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }


        }
    }

    /**
     * Class responsible to delete the event to server
     */
    class DeleteEventsAsyncTask extends AsyncTask<Integer, Integer, String> {

        private EventREST eventREST;
        private MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new MaterialDialog.Builder(getActivity())
                    .title(R.string.deleting)
                    .content(R.string.wait)
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(Integer... event) {
            eventREST = new EventREST();

            try {
                return eventREST.delete(event[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String events) {
            super.onPostExecute(events);
            dialog.dismiss();

            if (events == null)
                Snackbar.make(getView(), getResources().getString(R.string.m_server_problem),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            else {
                Snackbar.make(getView(), getResources().getString(R.string.m_delete_sucess),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                updateList();
            }


        }
    }
}
